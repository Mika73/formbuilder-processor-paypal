<?php namespace ProcessWire;
$info = [
'title' => 'Paypal payment action for FormBuilder',
'version' => 1,
'summary' => 'Collect payment after form submission using Paypal',
'icon' => 'cc-paypal',
'requires' => 'FormBuilder>=0.4.5',
];
