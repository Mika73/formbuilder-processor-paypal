<?php

namespace ProcessWire;

include_once(dirname(__DIR__) . '/FormBuilder/FormBuilderProcessorAction.php');

class FormBuilderProcessorPaypal extends FormBuilderProcessorAction implements Module, ConfigurableModule
{
  /**
   * Development mode?
   *
   */
  const dev = false;

  /**
   * @var null|int
   *
   */
  protected $saveFlags = null;

  /**
   * Construct
   *
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Action interface: Form ready to render
   * Captures payment for an order. To successfully capture payment for an order,
   * the buyer must first approve the order or a valid payment_source must be provided in the request.
   *
   * https://developer.paypal.com/docs/api/orders/v2/#orders_capture
   *
   */
  public function renderReady()
  {
    if ($this->isAdmin()) return;

    $this->devLog('renderReady');

    $input = $this->wire()->input;

    // Webhook
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

      $json_str = file_get_contents('php://input');
      $event = json_decode($json_str);

      $accessToken = $this->authentication();
      $verifyResp = $this->verifyWebhookSignature($accessToken, $event);

      if ($verifyResp->verification_status == 'SUCCESS') {

        $this->debugLog('webhook', $event->event_type);

        switch ($event->event_type) {

          case 'CHECKOUT.ORDER.APPROVED':
            $this->capturePayment($accessToken, $event, null);
            die('{1}');

          case 'CHECKOUT.ORDER.COMPLETED':
            $this->updateEntry($event);

            die('{1}');
        }
      }
    }

    if ($input->get('success') === 'true') {

      if ($input->get('token') && $input->get('PayerID')) {

        // LOCAL-SANDBOX mode
        if ($this->live < 0) {
          $orderId = $input->get('token');
          $accessToken = $this->authentication();
          $this->capturePayment($accessToken, $event, $orderId);
        }

        // replace form rendering with form success message
        $processor = $this->processor();
        $processor->addHookBefore('renderReady', function ($event) use ($processor) {
          $event->replace = true;
          $event->return =
            $processor->renderSubmittedLandmark() .
            $processor->renderSuccess($processor->successMessage);
        });
      }
      // buyer canceled the order
    } elseif ($input->get('success') === 'false') {

      $processor = $this->processor();
      $processor->addHookBefore('renderReady', function ($event) use ($processor) {
        $errorTemplate = isset($markup['error']) ? $markup['error'] : '';
        $event->replace = true;
        $event->return =
          $processor->renderSubmittedLandmark() .
          $processor->renderError($this->_('Payment failed'), $errorTemplate);
      });
    }
  }
  /**
   * Action interface: Form ready to process
   *
   */
  public function processReady()
  {
    if ($this->isAdmin()) return;
    $this->devLog('processReady');

    $processor = $this->processor;

    $processor->addHookBefore('saveForm', $this, 'hookBeforeSaveForm');
    $processor->addHookBefore('saveEntry', $this, 'hookBeforeSaveEntry');
    $processor->addHookBefore('renderSuccess', $this, 'hookBeforeRenderSuccess');
  }
  /**
   * Are we rendering/processing form in the admin?
   *
   * @return bool
   *
   */
  protected function isAdmin()
  {
    return $this->wire()->page->template->name === 'admin';
  }
  /**
   * Debug mode log
   *
   * @param string $name
   * @param string $value
   *
   */
  public function debugLog($name, $value)
  {
    if (!$this->debug) return;
    $ip = $this->wire()->session->getIP();
    $this->log("$ip $name: $value");
  }

  /**
   * Development mode log
   *
   * @param $value
   *
   */
  public function devLog($value)
  {
    if (self::dev) $this->debugLog('dev', $value);
  }

  /**
   * POST hook: Before FormBuilderProcessor::saveForm()
   *
   * Update $processor->saveFlags to be just saveFlagDB.
   * This way it just saves the entry to DB, but doesn’t send it anywhere else.
   *
   * @param HookEvent $event
   *
   */
  public function hookBeforeSaveForm(HookEvent $event)
  {
    if ($event) {
    }
    $processor = $this->processor();
    $submitType = $event->arguments(2);
    $isFinalSubmit = ($submitType & FormBuilderMaker::submitTypeFinal);

    if ($isFinalSubmit) {
      $this->debugLog('hookBeforeSaveForm', 'Updating saveFlags actions to only save entry to DB');
      $this->saveFlags = $processor->saveFlags;
      $processor->saveFlags = FormBuilderProcessor::saveFlagDB;
    } else {
      $this->debugLog('hookBeforeSaveForm', 'Not final submit');
    }
  }

  /**
   * POST hook: Before FormBuilderProcessor::saveEntry()
   *
   * Make $entry['entryFlags'] = FormBuilderEntries::flagPartial
   *
   * @param HookEvent $event
   *
   * @return void
   */
  public function hookBeforeSaveEntry(HookEvent $event)
  {
    /** @var array $entry */
    $entry = $event->arguments(0);
    $entry['entryFlags'] = FormBuilderEntries::flagPartial;
    $event->arguments(0, $entry);
    $this->debugLog('hookBeforeSaveEntry', 'Updating entry flags to be partial entry');
  }

  /**
   * Hook before and replace FormBuilderProcessor::renderSuccess() to change it to render a payment screen
   *  @param HookEvent $event
   */
  public function hookBeforeRenderSuccess(HookEvent $event)
  {
    $accessToken = $this->authentication();
    if ($accessToken) {
      $this->createOrder($accessToken);
    }
  }

  /**
   * PayPal REST APIs use OAuth 2.0 access tokens to authenticate requests.
   * Exchange client ID and secret for an access token.
   * https://developer.paypal.com/api/rest/authentication/
   * @return void
   */
  private function authentication()
  {
    if ($this->live > 0) {
      $clientId = $this->live_id;
      $clientSecret = $this->live_key;
      $url = "https://api-m.paypal.com/v1/oauth2/token";
    } else {
      $clientId = $this->sandbox_id;
      $clientSecret = $this->sandbox_key;
      $url = "https://api-m.sandbox.paypal.com/v1/oauth2/token";
    }

    $encode64 = base64_encode("$clientId:$clientSecret");
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
      "Authorization: Basic $encode64",
      "Content-Type: application/x-www-form-urlencoded",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    $data = "grant_type=client_credentials";

    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

    //for debug only!
    if ($this->live < 1) {
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    }

    $resp = json_decode(curl_exec($curl));
    curl_close($curl);

    if (isset($resp->access_token)) {
      return $resp->access_token;
    } else {
      return false;
    }
  }

  /**
   * Create order
   * An order represents a payment between two parties. Use the Orders API to create and capture orders.
   * https://developer.paypal.com/docs/api/orders/v2/
   * @param [type] $accessToken
   *
   * @return void
   */
  private function createOrder($accessToken)
  {
    $data = $this->createOrderData();
    $data = json_encode($data);
    $paypalApproveLink = '';

    if ($this->live > 0) {
      $paypalApiurl = "https://api-m.paypal.com/v2/checkout/orders";
    } else {
      $paypalApiurl = "https://api-m.sandbox.paypal.com/v2/checkout/orders";
    }

    $curl = curl_init($paypalApiurl);
    curl_setopt($curl, CURLOPT_URL, $paypalApiurl);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
      "Authorization: Bearer $accessToken",
      "Content-Type: application/json",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

    //for debug only!
    if ($this->live < 1) {
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    }
    $resp = json_decode(curl_exec($curl));
    curl_close($curl);

    foreach ($resp->links as $link) {
      if ($link->rel === 'approve') {
        $paypalApproveLink = $link->href;
        break;
      }
    }
    echo "<script>window.open('$paypalApproveLink', '_parent')</script>";
    exit;
  }

  /**
   * Set body of Create order Request
   * https://developer.paypal.com/docs/api/orders/v2/#orders-create-request-body
   *
   * @return void
   */
  private function createOrderData()
  {
    $url = $this->wire()->page->httpUrl();
    if ($this->wire()->page->template->name === 'form-builder') {
      $url = rtrim($url, '/') . '/' . $this->fbForm()->name . '/';
    }
    $returnUrl = $url;
    //TODO Delete the line below
    $returnUrl = 'https://6fcf-2a01-e0a-149-e2c0-da5c-9b55-30a7-c66a.eu.ngrok.io/form-builder/payment/';


    $purchaseUnits = $this->getPurchaseUnits();

    $data =
      [
        'intent' => 'CAPTURE',
        'purchase_units' => $purchaseUnits,
        'application_context' => [
          'return_url' => $returnUrl . '?success=true',
          'cancel_url' => $returnUrl . '?success=false'
        ]
      ];
    return $data;
  }
  /**
   * Set purchase_unit_request of Create order Request
   * https://developer.paypal.com/docs/api/orders/v2/#definition-purchase_unit_request
   *
   * @return void
   */
  public function ___getPurchaseUnits()
  {
    $price = $this->amount;
    $entry = $this->processor->getEntry();

    $description = $this->description;
    if (empty($description)) {
      $description = sprintf($this->_('Purchase from %s'), $this->wire()->config->httpHost);
    }

    $purchaseUnits = [
      [
        'reference_id' => $entry['id'],
        'amount' => [
          'currency_code' => $this->currency,
          'value' => $price
        ],
        'description' => $description
      ]
    ];
    return $purchaseUnits;
  }

  private function verifyWebhookSignature($accessToken, $event)
  {
    if ($this->live > 0) {
      $url = "https://api-m.paypal.com/v1/notifications/verify-webhook-signature";
      $webhookId = $this->liveWebhookId;
    } else {
      $url = "https://api-m.sandbox.paypal.com/v1/notifications/verify-webhook-signature";
      $webhookId = $this->testWebhookId;
    }

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
      "Authorization: Bearer $accessToken",
      "Content-Type: application/json",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    $data = json_encode([
      'auth_algo' => $_SERVER['HTTP_PAYPAL_AUTH_ALGO'],
      'cert_url' => $_SERVER['HTTP_PAYPAL_CERT_URL'],
      'transmission_id' => $_SERVER['HTTP_PAYPAL_TRANSMISSION_ID'],
      'transmission_sig' => $_SERVER['HTTP_PAYPAL_TRANSMISSION_SIG'],
      'transmission_time' => $_SERVER['HTTP_PAYPAL_TRANSMISSION_TIME'],
      'webhook_event' => $event,
      'webhook_id' => $webhookId,

    ]);

    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

    //for debug only!
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $verifyResp = json_decode(curl_exec($curl));
    curl_close($curl);

    return $verifyResp;
  }

  /**
   * Capture payment for order
   * https://developer.paypal.com/docs/api/orders/v2/#orders_capture
   *
   * @param [type] $orderId
   * @param [type] $accessToken
   *
   * @return void
   */
  private function capturePayment($accessToken, $event, $orderId)
  {
    if ($this->live < 0) {
      $url = "https://api-m.sandbox.paypal.com/v2/checkout/orders/$orderId/capture";
    } else {
      $links = $event->resource->links;
      $url = '';
      foreach ($links as $link) {
        if ($link->rel === 'capture') {
          $url = $link->href;
          break;
        }
      }
    }

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
      "Authorization: Bearer $accessToken",
      "Content-Type: application/json",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    $data = "{}";

    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

    //for debug only!
    if ($this->live < 1) {
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    }

    $event = json_decode(curl_exec($curl));
    curl_close($curl);

    // LOCAL-SANDBOX mode
    if ($this->live < 0) {
      $this->updateEntry($event);
    }
  }

  /**
   * Remove partial entry flag for entry
   *
   * @param [type] $event
   *
   * @return void
   */
  private function updateEntry($event)
  {
    $entryID = $event->purchase_units[0]->reference_id;
    $entryID = explode('-', $entryID);

    $entries = $this->processor()->entries();
    $entry = $entries->getById((int) $entryID[0]);
    $entry['entryFlags'] = $entry['entryFlags'] & ~FormBuilderEntries::flagPartial;
    $entry['payment_data'] = $event;

    $this->debugLog('chargeSuccess', "Removed partial entry flag for entry $entry[id]");

    $this->processor->entries()->save($entry);
    $this->processor->loadEntry($entry);
    $this->processor->saveForm($this->form(), $entry, FormBuilderMaker::submitTypeFinal);
  }
  /**
   * Get Inputfields to configure action
   *
   * @param InputfieldWrapper $inputfields
   *
   */
  public function getConfigInputfields(InputfieldWrapper $inputfields)
  {
    parent::getConfigInputfields($inputfields);
    $modules = $this->wire()->modules;

    if (!$this->live_id || !$this->live_key || !$this->sandbox_id || !$this->sandbox_key) {
      /** @var InputfieldMarkup $f */
      $f = $modules->get('InputfieldMarkup');
      $f->attr('name', '_need_paypal_api_keys');
      $f->value = '<p>' . $this->_('You must populate the Paypal API keys in the module settings before you can configure this.') . '</p>';
      $inputfields->add($f);
      return;
    }

    /** @var InputfieldRadios $f */
    $f = $modules->get('InputfieldRadios');
    $f->attr('name', 'live');
    $f->label = $this->_('Paypal mode');
    $f->icon = $this->live > 0 ? 'toggle-on' : 'toggle-off';
    $f->description =
      $this->_('The PayPal sandbox is a self-contained, virtual testing environment that simulates the live PayPal production environment. The sandbox provides a shielded space where you can initiate and watch while your apps process PayPal API requests without touching any live PayPal accounts.') . ' ';
    $f->notes =
      sprintf(
        $this->_('See also [PayPal sandbox testing guide](%s)'),
        'https://developer.paypal.com/tools/sandbox/'
      );
    $f->addOption(1, $this->_('LIVE mode: real money is charged'));
    $f->addOption(0, $this->_('SANDBOX mode: don’t charge real money'));
    $f->addOption(-1, $this->_('LOCAL-SANDBOX mode: don’t charge real money and don’t depend on Paypal webhooks'));

    $f->attr('value', (int) $this->live);
    $inputfields->add($f);

    /** @var InputfieldInteger $f */
    $f = $modules->get('InputfieldInteger');
    $f->attr('name', 'amount');
    $f->label = $this->_('Charge amount');
    $f->min = 1;
    $f->description =
      $this->_('Specify the charge amount in the currency’s unit.') . ' ' .
      $this->_('i.e. 1 is 1€ euro, 100 is 100€ euros.');

    $f->required = true;
    $f->attr('value', (int) $this->amount);
    $f->columnWidth = 50;
    $inputfields->add($f);

    /** @var InputfieldText $f */
    $f = $modules->get('InputfieldText');
    $f->attr('name', 'currency');
    $f->label = $this->_('Charge currency');
    $f->icon = 'money';
    $f->description = sprintf(
      $this->_('Specify one of the [currencies](%s) available from Paypal.'),
      'https://developer.paypal.com/api/rest/reference/currency-codes/'
    );
    $f->maxlength = 3;
    $f->attr('value', $this->currency);
    $f->columnWidth = 50;
    $inputfields->add($f);

    /** @var InputfieldText $f */
    $f = $modules->get('InputfieldText');
    $f->attr('name', 'description');
    $f->label = $this->_('Description');
    $f->description = $this->_('The purchase description.Minimum length: 1, Maximum length: 127.');
    $f->required = true;
    $f->minlength = 1;
    $f->maxlength = 127;
    $f->attr('value', $this->description);
    $f->columnWidth = 100;
    $inputfields->add($f);



    if (($this->live > -1)) {
      $webhookId = $this->createPaypalWebhook();

      /** @var InputfieldText $f */

      $f = $modules->get('InputfieldText');
      $f->label = $this->_('Webhook Id');
      $f->required = true;
      if (($this->live > 0)) {
        $f->attr('name', 'liveWebhookId');
      } else {
        $f->attr('name', 'testWebhookId');
      }

      $f->attr('value', $webhookId);

      $inputfields->add($f);
    }
  }
  /**
   * Create or update Paypal CHECKOUT.ORDER.COMPLETED webhook
   */
  protected function createPaypalWebhook()
  {
    $accessToken = $this->authentication();

    if ($this->live > 0) {
      $paypalApiurl = "https://api-m.paypal.com/v1/notifications/webhooks";
    } else {
      $paypalApiurl = "https://api-m.sandbox.paypal.com/v1/notifications/webhooks";
    }

    $url = $this->wire()->page->httpUrl();
    if ($this->wire()->page->template->name === 'form-builder') {
      $url = rtrim($url, '/') . '/' . $this->fbForm()->name . '/';
    }
    $returnUrl = $url;

    //TODO Delete the line below
    $returnUrl = 'https://6fcf-2a01-e0a-149-e2c0-da5c-9b55-30a7-c66a.eu.ngrok.io/form-builder/payment/';

    $curl = curl_init($paypalApiurl);
    curl_setopt($curl, CURLOPT_URL, $paypalApiurl);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
      "Authorization: Bearer $accessToken",
      "Content-Type: application/json",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    $data =
      [
        'url' => $returnUrl,
        'event_types' => [
          [
            'name' => 'CHECKOUT.ORDER.APPROVED'
          ],
          [
            'name' => 'CHECKOUT.ORDER.COMPLETED'
          ]
        ]
      ];
    $data = json_encode($data);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

    //for debug only!
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $result = json_decode(curl_exec($curl));
    $this->debugLog('Webhook Create result', json_encode($result));
    curl_close($curl);

    if (isset($result->name) && $result->name == 'WEBHOOK_URL_ALREADY_EXISTS') {
      if ($this->live > 0){
        return $this->liveWebhookId;
      }else{
        return $this->testWebhookId;
      }
    }

    return $result->id;
  }

  /**
   * Module config
   *
   * @param InputfieldWrapper $inputfields
   *
   */
  public function getModuleConfigInputfields(InputfieldWrapper $inputfields)
  {
    /** @var Modules $modules */
    $modules = $this->modules;

    /** @var InputfieldFieldset $fieldset */
    $fieldset = $modules->get('InputfieldFieldset');
    $fieldset->attr('name', '_paypal_keys');
    $fieldset->label = $this->_('Paypal API keys');
    $fieldset->icon = 'cc-paypal';
    $fieldset->description = $this->_('Paypal provides SANDBOX and LIVE versions of the API keys. Please populate them all here.');
    $fieldset->themeOffset = 1;
    $inputfields->add($fieldset);

    /** @var InputfieldText $f */
    $f = $modules->get('InputfieldText');
    $f->attr('name', 'live_id');
    $f->label = $this->_('Paypal LIVE client ID');
    $f->attr('value', $this->live_id);
    $f->required = true;
    $f->columnWidth = 50;
    $f->icon = 'key';
    $fieldset->add($f);

    /** @var InputfieldText $f */
    $f = $modules->get('InputfieldText');
    $f->attr('name', 'live_key');
    $f->label = $this->_('Paypal LIVE secret key');
    $f->attr('value', $this->live_key);
    $f->required = true;
    $f->columnWidth = 50;
    $f->icon = 'key';
    $fieldset->add($f);

    /** @var InputfieldText $f */
    $f = $modules->get('InputfieldText');
    $f->attr('name', 'sandbox_id');
    $f->label = $this->_('Paypal Sandbox client ID');
    $f->attr('value', $this->sandbox_id);
    $f->required = true;
    $f->columnWidth = 50;
    $f->icon = 'flask';
    $fieldset->add($f);

    /** @var InputfieldText $f */
    $f = $modules->get('InputfieldText');
    $f->attr('name', 'sandbox_key');
    $f->label = $this->_('Paypal Sandbox secret key');
    $f->attr('value', $this->sandbox_key);
    $f->required = true;
    $f->columnWidth = 50;
    $f->icon = 'flask';
    $fieldset->add($f);

    /** @var InputfieldFieldset $fieldset */
    $fieldset = $modules->get('InputfieldFieldset');
    $fieldset->attr('name', '_paypal_webhooks');
    $fieldset->label = $this->_('Paypal webhooks');
    $fieldset->icon = 'cc-paypal';
    $fieldset->themeOffset = 1;
    $inputfields->add($fieldset);

    $hasWebhooks = false;

    if ($this->sandbox_id && $this->sandbox_key) {
      $f = $this->getModuleConfigInputfieldsWebhooks('_x_test_webhooks', $this->_('TEST mode webhooks'));
      if ($f) {
        $fieldset->add($f);
        $hasWebhooks = true;
      }
    }
    if ($this->live_id && $this->live_key) {
      $f = $this->getModuleConfigInputfieldsWebhooks('_x_live_webhooks', $this->_('LIVE webhooks'));
      if ($f) {
        $fieldset->add($f);
        $hasWebhooks = true;
      }
    }
    if (!$hasWebhooks) $fieldset->notes = $this->_('This section will show webhooks added by forms when there are any to show.');

    /** @var InputfieldCheckbox $f */
    $f = $modules->get('InputfieldCheckbox');
    $f->attr('name', 'debug');
    $f->label = $this->_('Enable debug mode for verbose activity log in Setup > Logs > form-builder-processor-paypal');
    $f->themeOffset = 1;
    if ($this->debug) $f->attr('checked', 'checked');
    $inputfields->add($f);
  }

  /**
   * Get the webhooks configuration section for module config
   *
   * @param string $name
   * @param string $label
   * @return null|InputfieldCheckboxes Returns null if there are no webhooks
   *
   */
  protected function getModuleConfigInputfieldsWebhooks($name, $label)
  {
    $webhooks = $this->getPaypalWebhooks();

    if (!count($webhooks)) return null;

    /** @var InputfieldCheckboxes $f */
    $f = $this->wire()->modules->get('InputfieldCheckboxes');
    $f->attr('name', $name);
    $f->label = $label;
    $f->description =
      $this->_('Select any webhooks that you would like to DELETE.') . ' ' .
      $this->_('Delete webhooks that are redundant, no longer applicable, or if uninstalling this module.');

    $deletes = array();
    $deleteIds = $this->wire()->input->post($name);
    if (!is_array($deleteIds)) $deleteIds = array();

    foreach ($webhooks as $webhook) {
      if (in_array($webhook->id, $deleteIds, true)) {
        $deletes[$webhook->id] = $webhook;
      } else {
        /** @var \Stripe\WebhookEndpoint $webhook */
        $f->addOption($webhook->id, "$webhook->url [span.detail] ($webhook->id) [/span]");
      }
    }

    if (count($deletes)) {
      $this->deletePaypalWebhooks($deletes);
      $this->message(sprintf($this->_('Deleted %d Paypal webhook(s)'), count($deletes)));
    }

    return $f;
  }

  /**
   * Delete Paypal webhooks
   *
   * @param array $webhooks Array
   *
   */
  public function deletePaypalWebhooks(array $webhooks)
  {

    $accessToken = $this->authentication();

    foreach ($webhooks as $webhook) {
      if ($this->live > 0) {
        $paypalApiurl = "https://api-m.paypal.com/v1/notifications/webhooks/$webhook->id";
      } else {
        $paypalApiurl = "https://api-m.sandbox.paypal.com/v1/notifications/webhooks/$webhook->id";
      }
      $curl = curl_init($paypalApiurl);
      curl_setopt($curl, CURLOPT_URL, $paypalApiurl);
      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $headers = array(
        "Authorization: Bearer $accessToken",
        "Content-Type: application/json",
      );
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

      //for debug only!
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      curl_exec($curl);
    }
  }

  /**
   * Get all Paypal webhooks added by this module
   *
   * @return array
   */
  public function getPaypalWebhooks()
  {
    $accessToken = $this->authentication();

    if ($this->live > 0) {
      $paypalApiurl = "https://api-m.paypal.com/v1/notifications/webhooks";
    } else {
      $paypalApiurl = "https://api-m.sandbox.paypal.com/v1/notifications/webhooks";
    }

    $curl = curl_init($paypalApiurl);
    curl_setopt($curl, CURLOPT_URL, $paypalApiurl);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
      "Authorization: Bearer $accessToken",
      "Content-Type: application/json",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    //for debug only!
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = json_decode(curl_exec($curl));
    $webhooks = $resp->webhooks;

    curl_close($curl);
    return $webhooks;
  }
}
