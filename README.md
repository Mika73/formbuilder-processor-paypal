# ProcessWire FormBuilderProcessorPaypal
This module extends the commercial ProModule "[ProcessWire Form Builder](https://processwire.com/store/form-builder/)" and allows you to process credit card payments via the Paypal Checkout service along with your FormBuilder form.
## Requirements
- An active FormBuilder support subscription with FormBuilder v45 (0.4.5) or newer.
- ProcessWire core 3.0.164 or newer.
- Paypal account that is ready to use, along with the Client ID and the Secret to access it. The Client ID and the Secret are available in your Paypal DASHBOARD  in: Developers > My DASHBOARD & credentials.
- For the form(s) where you intend to use this module, they must have the “Save to entries database” action selected (as it is by default).
## How it works
When installed and properly configured on a FormBuilder form, the user fills out the form as usual and clicks the "submit" button when they are done. Assuming all required fields are present and validated, etc., it is time for the payment portion of the form. Rather than displaying a "thank you" or "success" message on the following screen, it instead queues the form to be submitted (saving it as a partial entry) and redirects to a Paypal Checkout payment URL.

Once the user completes their payment on the Paypal screen, Paypal redirects the user back to FormBuilder running on your website. At this time, they will see the "thank you" or "success" message from FormBuilder.  FormBuilder submits the queued form, converting it from a partial entry to a final entry, and executes any other actions configured for the form.

On the other hand, if the user cancelled the transaction, they are instead returned to the FormBuilder form as they left it, with the the fields still filled in. This gives the user another opportunity to complete their transaction, should that be their intention.

## Specifying charge info at runtime
Should you want the details of the charge (such as the amount) to be dictated at runtime rather than as a static setting, you can do this with a hook:
```
$wire->addHookAfter('FormBuilderProcessorPaypal::getPurchaseUnits', function ($e) {
 $referenceId =  $e['return'][0]['reference_id'];

   $purchaseUnits = [
    [
      'reference_id' => strval($referenceId).'-001',
      'amount' => [
        'currency_code' => 'EUR',
        'value' => "199.99" // i.e. 199.99 € EUR
      ],
      'description' =>  "Nintendo 3DS"
    ],
    [
      'reference_id' => strval($referenceId).'-002',
      'amount' => [
        'currency_code' => 'EUR',
        'value' => "899.99" // i.e. 899.99 € EUR
      ],
      'description' => "PlayStation 5"
    ]
  ]
;
  $e->return = $purchaseUnits;
});
```
